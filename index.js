/*S24 Activity Instructions
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.*/


const getCube = 2;
 console.log(`The cube of ${getCube} is ${getCube **3}`);


const address = {
    city: "258 Washington Ave NW",
    State: "California",
    postal: "90011"
}
console.log(`I live at ${address.city},${address.State} ${address.postal}`);


const animal = {
    name: "Lolong",
    weight: "1075 kgs",
    height: "20 ft 3 in"
}

console.log(`${animal.name} was a saltwater crocodile. He weighed at ${animal.weight} with a\nmeasurement of ${animal.height}`)


const numbers = [1, 2, 3, 4, 5, 15];
numbers.forEach(function(number) {
    console.log(number);
});























class dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed =breed;
    }
}

const myDog = new dog();
myDog.name= "Frankie";
myDog.age = "5"
myDog.breed = "Miniature Dachshund";
console.log(myDog);





